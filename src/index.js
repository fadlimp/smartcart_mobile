import React, {Component} from 'react';
import { Platform, StatusBar, StyleSheet, View, AppRegistry, NavigationContainer } from "react-native";
import {Root} from 'native-base';
import Navigator from '_navigations';

import { createStore, applyMiddleware } from 'redux';
import { Provider, connect } from "react-redux";

import { ThemeColors } from 'react-navigation';
import * as theme from './constants/theme';
class App extends Component {
		state = {
			isLoadingComplete: false
		};
		_loadResourcesAsync = async () => {
			return Promise.all([
				Asset.loadAsync([require("./assets/images/Base/Logo.png")]),
				Font.loadAsync({
					// This is the font that we are using for our tab bar
					// ...Icon.Ionicons.font,
					"CeraPro-Regular"       : require("./assets/fonts/CeraPro-Regular.ttf"),
					"CeraPro-Bold"       : require("./assets/fonts/CeraPro-Bold.ttf"),
					"CeraPro-Light"       : require("./assets/fonts/CeraPro-Light.ttf"),
					"Amplesoft-Pro"       : require("./assets/fonts/Amplesoft-Pro.otf")
				})
			]);
		};
	
		_handleLoadingError = error => {
			// In this case, you might want to report the error to your error
			// reporting service, for example Sentry
			console.warn(error);
		};
	
		_handleFinishLoading = () => {
			this.setState({ isLoadingComplete: true });
		};
		render() {
				
			return (
					
				<Root>
					<View style={styles.container}>
					{Platform.OS === "ios" && <StatusBar barStyle="default" />}
					<Navigator />
					</View>
				</Root>
						
			);
		}
	}
export default App

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: theme.colors.body
	}
});

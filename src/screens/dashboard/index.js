import React, { Component } from 'react';
import { 
	TouchableOpacity, 
	Image, 
	SafeAreaView, 
	ScrollView, 
	StyleSheet,
	Dimensions, 
	View
} from 'react-native';
import { Block, Badge, Text, CustomIcon} from '_atoms';
import * as theme from '../../constants/theme';

import hexToRgba from "hex-to-rgba";
import Carousel from 'react-native-snap-carousel';
import FlipCard from 'react-native-flip-card';


const { width: viewportWidth } = Dimensions.get('window');

global.currentScreenIndex = 0;

class Dashboard extends Component {


	constructor() {
		super()
		this.state = {
		  	entries: [
				{ 
					title: '312',
					subtitle: 'Active Promo',
					icon: <CustomIcon activePromo />
				},
				{ 
					title: '78',
					subtitle: 'Broadcaster', 
					icon: <CustomIcon broadcaster />
				},
				{ 
					title: '8.702',
					subtitle: 'Total Redeem', 
					icon: <CustomIcon totalRedeem />
				},
				{ 
					title: '16.220',
					subtitle: 'Total User', 
					icon: <CustomIcon totalUser />
				}
			],
			flip: false
		}
	}
	
	_renderItem = ({item, index}) => {
        return (
            <View style={styles.cardSlider}>
				{ item.icon }
				<Text h2 style={{ marginTop: 17 }}>{ item.title }</Text>
				<Text paragraph color="gray">{ item.subtitle }</Text>
			</View>
        );
	}
	
	
	static navigationOptions = ({ navigation }) => ({
		headerTransparent: false,
		headerStyle: {
			borderBottomWidth: 0
		},
		headerLeftContainerStyle: {
			paddingLeft: 24
		},
		headerRightContainerStyle: {
			paddingRight: 24
		},
		headerTitleContainerStyle: {
			paddingTop: 15
		},
		headerLeft: (
			<TouchableOpacity onPress={() => navigation.openDrawer()}>
				<CustomIcon menu />
			</TouchableOpacity>
		),
		headerRight: (
			<TouchableOpacity onPress={()=>{
				navigation.navigate('Auth')
				
			}}><CustomIcon notification /></TouchableOpacity>
		),
		headerTitle: (
			<Block row middle>
				<Image
					source={require('../../assets/images/Base/Logo.png')}
					style={styles.mainLogo}
				/>
			</Block>
		)
	});
	
	_renderItem = ({item, index}) => {
		
        return (
			<View style={styles.awardBox}>
				<View style={styles.awardImageBox}>
					{ item.image }
					
				</View>	
				<View style={styles.awardContent}>
					<Text h4 bold style={styles.awardText}>
						{ item.title }
					</Text>
					<Block row center>
						<CustomIcon target />
						<Text h3 bold color="red"> { item.point }</Text>
					</Block>
				</View>

				<View style={styles.voteAwardButton}>
					<Text h4 color="white">vote</Text>
				</View>
			</View>	
        );
    }

	renderTripButton() {
		const { navigation } = this.props;
	
		return (
		  <Block center middle style={styles.endTrip}>
			<Badge color={hexToRgba(theme.colors.blue, "0.32")} size={112}>
			  <TouchableOpacity
				activeOpacity={0.8}
				onPress={() => navigation.navigate("")}
			  >
				<Badge color={theme.colors.blue} size={88}>
					<CustomIcon payButton />
				</Badge>
			  </TouchableOpacity>
			</Badge>
		  </Block>
		);
	}


	render() {

		const entries = [{ 
			title: 'Ding Ding, its lunch time! Which one do you prefer',
			point: '200',
			image: <Image source={require('../../assets/images/Base/award-1.jpg')}/>
		},
		{	
			title: 'Psst! have you seen this video before ?',
			point: '120',
			image: <Image source={require('../../assets/images/Base/award-2.jpg')}/>
		}];

		const { navigation } = this.props

		return (
			<SafeAreaView style={styles.Dashboard}>
				<ScrollView contentContainerStyle={{ paddingVertical: 25 }}>

					<View style={styles.accent1}>
						<Image
							source={require('../../assets/images/Base/accent-top-2.png')}
							style={styles.accentImage}
						/>
					</View>

					<Block style={styles.headerWrapper}>
						<FlipCard 
							style={styles.card}
							friction={6}
							perspective={1000}
							flipHorizontal={true}
							flipVertical={false}
							flip={false}
							clickable={true}
							>
							{/* Face Side */}
							<View style={styles.cardFace}>
								<Image source={require('../../assets/images/Base/card-template.png')}
								style={styles.cardBackground}/>
								<Text bold style={styles.cardName}>Welcome</Text>
							</View>
							{/* Back Side */}
							<View style={styles.cardFace}>
								<Image source={require('../../assets/images/Base/card-template2.png')}
								style={styles.cardBackground}/>
								<Image source={require('../../assets/images/Base/qr.jpg')}
								style={styles.cardQr}/>
							</View>
						</FlipCard>
					</Block>
					
					<View style={styles.margin}>

						<Block row style={styles.topButtonFrame}>
							<Block style={[styles.topButtonBox, styles.bg1]}>
								<TouchableOpacity 
										activeOpacity={0.8} 
										style={{margin:0, padding:0}}
										onPress={() => console.log("today receipt page")}>
								<Image source={require('../../assets/images/Base/cartoon-1.png')}
								style={styles.topButtonImage}/>
								<View style={styles.topButtonText}>
									<Text bold color="white">Today</Text>
									<Text color="white">Receipts</Text>
								</View>
								<View style={styles.notificationCircle}>
									<Text bold color="white">7</Text>
								</View>
								</TouchableOpacity>
							</Block>	
							<Block style={[styles.topButtonBox, styles.bg2]}>
								<TouchableOpacity 
										activeOpacity={0.8} 
										style={{margin:0, padding:0}}
										onPress={() => console.log("my point page")}>
								<Image source={require('../../assets/images/Base/cartoon-2.png')}
								style={styles.topButtonImage}/>
								<View style={styles.topButtonText}>
									<Text bold color="white">My</Text>
									<Text color="white">Points</Text>
								</View>
								</TouchableOpacity>
							</Block>

								
								<Block style={[styles.topButtonBox, styles.bg3]}>
									<TouchableOpacity 
										activeOpacity={0.8} 
										style={{margin:0, padding:0}}
										onPress={() => this.props.navigation.navigate('History')}>
									<Image source={require('../../assets/images/Base/cartoon-3.png')}
									style={styles.topButtonImage}/>
									<View style={styles.topButtonText}>
										<Text bold color="white">Shopping</Text>
										<Text color="white">History</Text>
									</View>
									</TouchableOpacity>
								</Block>
						</Block>
					</View>

					<View style={styles.roundedTopGreen}></View>
					<View style={styles.bodyGreen}>
						<View style={styles.margin}>
							<View style={styles.titleHeader}>
								<Text bold h3 color="white">Earn </Text> 
								<Text light h3 color="white">Points</Text>
							</View>
						
							<Block row center>
								
								<TouchableOpacity 
								activeOpacity={0.8} 
								style={[styles.pointsBox, styles.socmedBox]}
								onPress={() => this.props.navigation.navigate('ShareSmartcart')}>
								
									<Image source={require('../../assets/images/Base/socmed-logo.png')}
									style={styles.socmedLogo}/>
									<View style={styles.textBox}>
										
										<Text h3>Share</Text>
										<Text color="grey">Get 45 Points</Text>
									</View>
								</TouchableOpacity>	
								
								<TouchableOpacity 
								activeOpacity={0.8} 
								style={[styles.pointsBox, styles.videoBox]}
								onPress={() => this.props.navigation.navigate('WatchVideo')}>
									<Image source={require('../../assets/images/Base/cartoon-file.png')}
									style={styles.watchLogo}/>
									<View style={styles.textBox}>
										<Text h3>Watch</Text>
										<Text color="grey">Get 30 Points</Text>
									</View>
								</TouchableOpacity>		
							</Block>
						</View>	
					</View>
					<View style={[styles.roundedTopGreen, styles.backgroundWhite]}></View>
					<View style={styles.margin}>
						<View style={styles.titleHeader}>
							<Text bold h3>Mega </Text>
							<Text light h3>Awards</Text>
						</View>
					</View>
					<View style={styles.awardSlider}>
						<Carousel
							ref={(c) => { this._carousel = c; }}
							data={entries}
							renderItem={this._renderItem}
							activeSlideAlignment={'start'}
							sliderWidth={viewportWidth}
							itemWidth={330}
							inactiveSlideOpacity={1}
							inactiveSlideScale={1}
							style={{zIndex: 3}}
						/>
				
					</View>
					<View style={styles.accent2}>
						<Image
							source={require('../../assets/images/Base/accent-bottom.png')}
							style={{flex:1}}
						/>
					</View>		
				</ScrollView>
				{this.renderTripButton()}
			</SafeAreaView>
		)
	}
}


const styles = StyleSheet.create({
	endTrip: {
		position: "absolute",
		left: (viewportWidth - 112) / 2,
		bottom: -20,
		elevation: 2,
	},
	awardSlider: {
		marginVertical: 30,
		flexDirection: "row",
		justifyContent: "center",
		paddingBottom: 80,
		zIndex: 3
	},
	awardBox: {
		width: 300,
		borderRadius: 16,
		elevation: 2,
		marginHorizontal: 20,
		marginVertical: 20,
		backgroundColor: theme.colors.white
	},
	awardImageBox: {
		flex: 1,
		height: 150,
		borderRadius: 16,
		borderBottomRightRadius: 0,
		overflow: "hidden",
	},
	awardContent: {
		flex: 1,
		alignContent: 'center',
		padding: 20,
	},
	awardText: {
		paddingBottom: 10
	},
	voteAwardButton:{ 
		position: "absolute",
		bottom: -22,
		right: 0,
		paddingHorizontal: 30,
		paddingVertical: 10,
		backgroundColor: theme.colors.primary,
		alignContent: "center",
		justifyContent: "center",
		borderRadius: 30,
		borderTopRightRadius: 4
	},
	Dashboard: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: theme.colors.body
	},
	accent1: {
		height: 200,
		position: 'absolute',
		top: 0,
		right: 0,
		alignSelf: 'flex-start'
	},
	accent2: {
		height: 400,
		position: 'absolute',
		bottom: 0,
		left: 0,
		zIndex: 1
	},
	accentImage: {
		width: 150
	},
	margin: {
		marginHorizontal: 24,
		zIndex: 2
	},
	cardSlider: {
		marginLeft: 18,
		backgroundColor: '#218978',
		shadowRadius: 16,
		padding: 20,
		borderRadius: 16,
		elevation: 1
	},
	mainLogo: {
		width: 140,
		height: 28,
		marginTop: -5
	},
	card: {
		width: viewportWidth - 40,
		maxWidth: 360,
		height: 230,
		borderRadius: 22,
		overflow: "hidden",
		alignSelf: "center",
		justifyContent: "center",
		marginTop: 20
	},
	cardBackground: {
		flex:1,
		position: "absolute",
	},
	cardName: {
		paddingTop: 120,
		fontSize: 20,
		alignSelf: "flex-start",
		paddingLeft: 25,
		color: "#ffffff"
	},
	cardQr: {
		width: 160,
		height: 160,
		alignSelf: "center",
		borderRadius: 16,
		overflow: "hidden",
		marginTop: 20
	},
	topButtonFrame: {
		marginVertical: 40,
		height: 130,
	},
	topButtonBox: {
		borderRadius: 16,
		flex: 1,
		marginHorizontal: 8,
		backgroundColor: "#8293DD",
	},
	topButtonImage: {
		width: 66,
		height: 66,
		borderBottomRightRadius: 16,
		overflow: "hidden",
		position: "absolute",
		opacity: 0.8,
		bottom: -80,
		right: 0,
	},
	topButtonText: {
		fontSize: 16,
		color: '#ffffff',
		paddingLeft: 14,
		paddingTop: 14
	},
	bg1: {
		backgroundColor: "#8293DD"
	},
	bg2: {
		backgroundColor: "#BC7BB7",
	},
	bg3: {
		backgroundColor: '#6BCBCB'
	},
	notificationCircle: {
		width: 34,
		height: 34,
		backgroundColor: theme.colors.red,
		borderRadius: 20,
		borderWidth: 4,
		borderColor: '#ffffff',
		justifyContent: 'center',
		alignItems: 'center',
		position: "absolute",
		top: -15,
		right: -15
	},
	roundedTopGreen: {
		borderTopLeftRadius: 32,
		borderTopRightRadius: 32,
		backgroundColor: theme.colors.primary,
		height: 50
	},
	bodyGreen: {
		backgroundColor: theme.colors.primary,
		paddingBottom: 30
	},
	titleHeader: {
		display: 'flex',
		flexDirection: 'row'
	},
	pointsBox: {
		elevation: 2,
		flex: 2,
		height: 200,
		marginVertical: 30,
		marginHorizontal: 10,
		borderRadius: 16,
		overflow: "hidden",
		padding: 20,
		backgroundColor: theme.colors.white,
	},
	socmedLogo: {
		width: 110,
		height: 40
	},
	watchLogo: {
		width: 70,
		height: 70
	},
	textBox: {
		position: "absolute",
		bottom: 30,
		left: 20
	},
	backgroundWhite: {
		backgroundColor: "#ffffff",
		marginTop: -30
	}
});


export default Dashboard;

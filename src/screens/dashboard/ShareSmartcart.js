import React, { Component } from 'react';
import { 
	TouchableOpacity, 
	Image, 
	SafeAreaView, 
	ScrollView, 
	StyleSheet,
	Dimensions, 
	View
} from 'react-native';
import { Block, Badge, Text, CustomIcon} from '_atoms';
import Icon from 'react-native-vector-icons/FontAwesome';
import * as theme from '../../constants/theme';

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

class ShareSmartcart extends Component {
	
	static navigationOptions = ({ navigation }) => ({
		headerTransparent: true,
		headerStyle: {
			borderBottomWidth: 0
		},
		headerLeftContainerStyle: {
			paddingLeft: 24
		},
		headerRightContainerStyle: {
			paddingRight: 24
		},
		headerTitleContainerStyle: {
			paddingTop: 15
		},
		headerLeft: (
			<TouchableOpacity onPress={() => navigation.goBack()}>
				<CustomIcon back />
			</TouchableOpacity>
		)
	});
	
	
	render() {
		return (
			<SafeAreaView>
				<ScrollView>
                    <View  style={styles.body}>
                        <View style={styles.accent1}>
                            <Image
                                source={require('../../assets/images/Base/accent-top.png')}
                                style={styles.accentImage}
                            />
                        </View>
                        <View style={styles.margin}>
                            <View style={styles.titleHeader}>
                                <Text h2 bold>Share Smartcart</Text>
                            </View>
                            <Block style={{paddingTop:40}}>
                                <Block row middle style={[styles.shareButton, styles.facebook]}>
                                    <View style={{width: 50,alignItems:"center", justifyContent: "center"}}>
                                        <Icon name="facebook" size={35} color="#fff"/>
                                    </View>
                                    <Block row center flex={5}>
                                        <Text light h4 color="white">
                                             Share on facebook gets 15 points 
                                        </Text>
                                    </Block>
                                    <Block center middle flex={1.5}>
                                        <Icon name="circle-o" size={25} color="#fff"/>
                                    </Block>
                                </Block>
                                <Block row middle style={[styles.shareButton, styles.twitter]}>
                                    <View style={{width: 50,alignItems:"center", justifyContent: "center"}}>
                                        <Icon name="twitter" size={35} color="#fff"/>
                                    </View>
                                    <Block row center flex={5}>
                                        <Text light h4 color="white">
                                             Share on facebook gets 15 points 
                                        </Text>
                                    </Block>
                                    <Block center middle flex={1.5}>
                                        <Icon name="circle-o" size={25} color="#fff"/>
                                    </Block>
                                </Block>
                                <Block row middle style={[styles.shareButton, styles.instagram]}>
                                    <View style={{width: 50,alignItems:"center", justifyContent: "center"}}>
                                        <Icon name="instagram" size={35} color="#fff"/>
                                    </View>
                                    <Block row center flex={5}>
                                        <Text light h4 color="white">
                                             Share on facebook gets 15 points 
                                        </Text>
                                    </Block>
                                    <Block center middle flex={1.5}>
                                        <Icon name="circle-o" size={25} color="#fff"/>
                                    </Block>
                                </Block>
                            </Block>
                        </View>
                    </View>    
				</ScrollView>
			</SafeAreaView>
		)
	}
}


const styles = StyleSheet.create({
    body: { 
        minHeight: viewportHeight,
    },
    margin: {
        marginHorizontal: 24
    },
    titleHeader: {
        marginTop: 120,

    },  
	accent1: {
		height: 200,
		position: 'absolute',
		top: 0,
		alignSelf: 'flex-start'
	},
	accentImage: {
		width: viewportWidth
    },
    shareButton: {
        marginBottom: 20,
        borderRadius: 10,
        elevation: 2,
        paddingHorizontal: 20,
        paddingVertical: 25
    },
    facebook: {
        backgroundColor: "#4481BE"
    },
    twitter: {
        backgroundColor: "#60C4F8"
    },
    instagram: {
        backgroundColor: '#AF6EBA'
    },  
    checkFrame: {
        width: 80
    }
});


export default ShareSmartcart;

import React, { Component } from 'react';
import { 
	TouchableOpacity, 
	SafeAreaView, 
	ScrollView, 
	StyleSheet,
	View,
	Image
} from 'react-native';
import { Block, Card, Text, Label } from '_atoms';
import * as theme from '../../constants/theme';
import { createStackNavigator } from "react-navigation-stack";

class History extends Component {
	
	static navigationOptions = ({ navigation }) => ({
		headerStyle: {
			backgroundColor: theme.colors.primary,
			shadowColor: 'rgba(55,55,55,0)',
			elevation: 0
		},
		headerLeftContainerStyle: {
			paddingLeft: 24
		},
		headerRightContainerStyle: {
			paddingRight: 24
		},
		headerTitleContainerStyle: {
			paddingTop: 15
		},
		headerLeft: (
			<TouchableOpacity onPress={() => navigation.openDrawer()}>
				{/* <Icon menu /> */}
			</TouchableOpacity>
		),
		headerTitle: (
			<Block row middle></Block>
		)
	});
	

	render() {
		return (
			<SafeAreaView style={styles.bgbody}>
				<ScrollView contentContainerStyle={{ paddingVertical: 0 }}
				style={styles.bgbodyWhite}>
					<View style={styles.BgWrapper}>
						<Text h3 style={styles.headerTitle}>History.</Text>
						<Image
							source={require('../../assets/images/header-background.png')}
							style={styles.headerBG}
						/>
						<View style={styles.roundedFooter} />
					</View>

					<TouchableOpacity activeOpacity={0.75} 
										onPress={() => this.props.navigation.navigate('detailHistory')}>
						<Card style={[styles.margin]}>
							<Block row center middle>
								<Block flex={10}>
									<Text captionDisabled>-status-</Text>
									<Text h4 style={styles.titleCamp}>Hero Supermarket</Text>
									<Block row center>
										<Block flex={0.12}>
											{/* <Icon style={styles.iconCamp} calendar /> */}
										</Block>
										<Block>
											<Text style={styles.dateCamp}  color="gray3">
												29 Jun 2020
											</Text>
										</Block>
									</Block>
								</Block>
								<Block center>
									{/* <Icon arrowRight /> */}
								</Block>
							</Block>		
						</Card>
					</TouchableOpacity>	
					<TouchableOpacity activeOpacity={0.75}>
						<Card style={[styles.margin]}>
							<Block row center middle>
								<Block flex={10}>
									<Text caption>-status-</Text>
									<Text h4 style={styles.titleCamp}>Giant Supermarket</Text>
									<Block row center>
										<Block flex={0.12}>
											{/* <Icon style={styles.iconCamp} calendar /> */}
										</Block>
										<Block>
											<Text style={styles.dateCamp}  color="gray3">
												29 March 2020
											</Text>
										</Block>
									</Block>
								</Block>
								<Block center>
									{/* <Icon arrowRight /> */}
								</Block>
							</Block>		
						</Card>
					</TouchableOpacity>	
					<TouchableOpacity activeOpacity={0.75}>
						<Card style={[styles.margin]}>
							<Block row center middle>
								<Block flex={10}>
									<Text captionDisabled>-status-</Text>
									<Text h4 style={styles.titleCamp}>Hero Supermarket</Text>
									<Block row center>
										<Block flex={0.12}>
											{/* <Icon style={styles.iconCamp} calendar /> */}
										</Block>
										<Block>
											<Text style={styles.dateCamp}  color="gray3">
												29 Jun 2020
											</Text>
										</Block>
									</Block>
								</Block>
								<Block center>
									{/* <Icon arrowRight /> */}
								</Block>
							</Block>		
						</Card>
					</TouchableOpacity>	
					<TouchableOpacity activeOpacity={0.75}>
						<Card style={[styles.margin]}>
							<Block row center middle>
								<Block flex={10}>
									<Text caption>-status-</Text>
									<Text h4 style={styles.titleCamp}>Giant Supermarket</Text>
									<Block row center>
										<Block flex={0.12}>
											{/* <Icon style={styles.iconCamp} calendar /> */}
										</Block>
										<Block>
											<Text style={styles.dateCamp}  color="gray3">
												29 March 2020
											</Text>
										</Block>
									</Block>
								</Block>
								<Block center>
									{/* <Icon arrowRight /> */}
								</Block>
							</Block>		
						</Card>
					</TouchableOpacity>	
					<TouchableOpacity activeOpacity={0.75}>
						<Card style={[styles.margin]}>
							<Block row center middle>
								<Block flex={10}>
									<Text captionDisabled>-status-</Text>
									<Text h4 style={styles.titleCamp}>Hero Supermarket</Text>
									<Block row center>
										<Block flex={0.12}>
											{/* <Icon style={styles.iconCamp} calendar /> */}
										</Block>
										<Block>
											<Text style={styles.dateCamp}  color="gray3">
												29 Jun 2020
											</Text>
										</Block>
									</Block>
								</Block>
								<Block center>
									{/* <Icon arrowRight /> */}
								</Block>
							</Block>		
						</Card>
					</TouchableOpacity>	
					<TouchableOpacity activeOpacity={0.75}>
						<Card style={[styles.margin]}>
							<Block row center middle>
								<Block flex={10}>
									<Text caption>-status-</Text>
									<Text h4 style={styles.titleCamp}>Giant Supermarket</Text>
									<Block row center>
										<Block flex={0.12}>
											{/* <Icon style={styles.iconCamp} calendar /> */}
										</Block>
										<Block>
											<Text style={styles.dateCamp}  color="gray3">
												29 March 2020
											</Text>
										</Block>
									</Block>
								</Block>
								<Block center>
									{/* <Icon arrowRight /> */}
								</Block>
							</Block>		
						</Card>
					</TouchableOpacity>	

				</ScrollView>
			</SafeAreaView>
		)
	}
}


export default History;

const styles = StyleSheet.create({
	bgbody: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: theme.colors.body,
	},
	bgbodyWhite: {
		backgroundColor: theme.colors.body,
	},
	margin: {
		marginHorizontal: 20,
		marginBottom: 10,
	},
	headerWrapper: {
		marginVertical: 30,
		marginHorizontal: 30,
	},
	headerTitle: {
		color: theme.colors.white,
		zIndex: 2,
		marginVertical: 20,
		marginHorizontal: 30

	},	
	titleCamp: {
		marginTop: 10,
		marginBottom: 5
	},
	iconCamp: {
		display: 'flex'
	},
	dateCamp: {
		display: 'flex'
	},
	BgWrapper: {
		height: 130,
        flexDirection: 'column',
        justifyContent: 'flex-end',
		backgroundColor: '#10A98F'
	},
	roundedFooter: {
		backgroundColor: theme.colors.body,
		height: 32,
		borderTopRightRadius: 32,
		borderTopLeftRadius: 32,
		shadowRadius: 12,
		zIndex: 2
	},
	headerBG: {
		position: "absolute",
		bottom: 0,
		right: 0,
		zIndex: 0,
		width: 250,
		opacity: 0.5
	}
});

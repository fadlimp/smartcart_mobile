import React, { Component } from 'react';
import { 
	TouchableOpacity, 
	SafeAreaView, 
	ScrollView, 
	StyleSheet,
	View,
	Image
} from 'react-native';
import { Block, Card, Text, Label } from '_atoms';
import * as theme from '../../constants/theme';
import { createStackNavigator } from "react-navigation-stack";

class detailHistory extends Component {
	
	static navigationOptions = ({ navigation }) => ({
		headerStyle: {
			backgroundColor: theme.colors.primary,
			shadowColor: 'rgba(55,55,55,0)',
			elevation: 0
		},
		headerLeftContainerStyle: {
			paddingLeft: 24
		},
		headerRightContainerStyle: {
			paddingRight: 24
		},
		headerTitleContainerStyle: {
			paddingTop: 15
		},
		headerLeft: (
			<TouchableOpacity onPress={() => navigation.openDrawer()}>
				{/* <Icon menu /> */}
			</TouchableOpacity>
		),
		headerTitle: (
			<Block row middle></Block>
		)
	});
	

	render() {
		return (
			<SafeAreaView style={styles.bgbody}>
				<ScrollView contentContainerStyle={{ paddingVertical: 0 }}
				style={styles.bgbodyWhite}>
					<View style={styles.BgWrapper}>
						<Text h3 style={styles.headerTitle}>Detail History.</Text>
						{/* <Image
							source={require('../../assets/images/header-background.png')}
							style={styles.headerBG}
						/> */}
						<View style={styles.roundedFooter} />
					</View>
          <Card
						middle title="detail transaction"
						style={[styles.margin, { marginTop: 18 }]}
					>
						<Block style={styles.topPromo}>
							<TouchableOpacity activeOpacity={0.8}>
								<Block row center>
									<Block>
										{/* <Image
											style={styles.campaignPict}
											source={{ uri: 'https://telegram-bot-s3.s3-ap-southeast-1.amazonaws.com/promo/b4206fa-fd88-7f74-f82b-2f8fdcc7e048.png' }}
										/> */}
									</Block>
									<Block flex={2}>
										<Text h4>Product Name</Text>
										<Text light color="gray">category</Text>
									</Block>
									<Block>
										<Text paragraph right color="black">Rp. 10.000</Text>
										<Text paragraph right color="gray">qty. 1</Text>
									</Block>
								</Block>
							</TouchableOpacity>
						</Block>
						<Block style={styles.topPromo}>
							<TouchableOpacity activeOpacity={0.8}>
								<Block row center>
									<Block>
										{/* <Image
											style={styles.campaignPict}
											source={{ uri: 'https://telegram-bot-s3.s3-ap-southeast-1.amazonaws.com/promo/171c6cc-3b5-27f6-8e50-573a1408b676.jpeg' }}
										/> */}
									</Block>
									
									<Block flex={2}>
										<Text h4>Product Name</Text>
										<Text light color="gray">category</Text>
									</Block>
									<Block>
										<Text paragraph right color="black">Rp. 10.000</Text>
										<Text paragraph right color="gray">qty. 1</Text>
									</Block>
								</Block>
							</TouchableOpacity>
						</Block>
						<Block style={styles.topPromo}>
							<TouchableOpacity activeOpacity={0.8}>
								<Block row center>
									<Block>
										{/* <Image
											style={styles.campaignPict}
											source={{ uri: 'https://telegram-bot-s3.s3-ap-southeast-1.amazonaws.com/promo/1dbb0-16d6-0535-8b8a-636031e017b.jpeg' }}
										/> */}
									</Block>
									<Block flex={2}>
										<Text h4>Product Name</Text>
										<Text light color="gray">category</Text>
									</Block>
									<Block>
										<Text paragraph right color="black">Rp. 10.000</Text>
										<Text paragraph right color="gray">qty. 1</Text>
									</Block>
								</Block>
							</TouchableOpacity>
						</Block>
							
							{/* <HighchartsReactNative
								styles={styles.chartContainer}
								options={this.state.chartOptions}
							/> */}	
					</Card>

				</ScrollView>
			</SafeAreaView>
		)
	}
}


export default detailHistory;


const styles = StyleSheet.create({
	bgbody: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: theme.colors.body,
	},
	bgbodyWhite: {
		backgroundColor: theme.colors.body,
	},
	margin: {
		marginHorizontal: 20,
		marginBottom: 10,
	},
	headerWrapper: {
		marginVertical: 30,
		marginHorizontal: 30,
	},
	headerTitle: {
		color: theme.colors.white,
		zIndex: 2,
		marginVertical: 20,
		marginHorizontal: 30

	},	
	titleCamp: {
		marginTop: 10,
		marginBottom: 5
	},
	iconCamp: {
		display: 'flex'
	},
	dateCamp: {
		display: 'flex'
	},
	BgWrapper: {
		height: 130,
        flexDirection: 'column',
        justifyContent: 'flex-end',
		backgroundColor: '#10A98F'
	},
	roundedFooter: {
		backgroundColor: theme.colors.body,
		height: 32,
		borderTopRightRadius: 32,
		borderTopLeftRadius: 32,
		shadowRadius: 12,
		zIndex: 2
	},
	headerBG: {
		position: "absolute",
		bottom: 0,
		right: 0,
		zIndex: 0,
		width: 250,
		opacity: 0.5
	}
});


import React, { PureComponent } from 'react';
import { View, Image } from 'react-native';

const menuIcon = (
  <View style={{padding: 15, marginLeft: -15, backgroundColor: 'rgba(0,0,0,0)'}}>
    <Image
      source={require('../../assets/images/icons/menu.png')}
      style={{ height: 17, width: 24 }}
    />
  </View>
);

const notificationIcon = (
  <Image
    source={require('../../assets/images/icons/notifications.png')}
    style={{ height: 22, width: 21 }}
  />
);
const powerIcon = (
  <View style={{padding: 15, marginRight: -10}}>
    <Image
      source={require('../../assets/images/icons/power.png')}
      style={{ height: 22, width: 21 }}
    />
  </View>  
);

const ActivePromoIcon = (
  <View style={{ 
    padding: 12, 
    paddingTop: 22, 
    backgroundColor: '#2DD0B4', 
    elevation: 1, 
    borderRadius: 16, 
    borderTopLeftRadius:0, 
    borderTopRightRadius:0, 
    width: 56 }}
  >
    <Image
      source={require('../../assets/images/icons/home-icon-1.png')}
      style={{ height: 32, width: 32 }}
    />
  </View>
);
const BroadcasterIcon = (
  <View style={{ 
    padding: 12, 
    paddingTop: 22, 
    backgroundColor: '#2DD0B4', 
    elevation: 1, 
    borderRadius: 16, 
    borderTopLeftRadius:0, 
    borderTopRightRadius:0, 
    width: 56 }}
  > 
    <Image
      source={require('../../assets/images/icons/home-icon-2.png')}
      style={{ height: 32, width: 32 }}
    />
  </View>
);
const TotalRedeemIcon = (
  <View style={{ 
    padding: 12, 
    paddingTop: 22, 
    backgroundColor: '#2DD0B4', 
    elevation: 1, 
    borderRadius: 16, 
    borderTopLeftRadius:0, 
    borderTopRightRadius:0, 
    width: 56 }}
  >
    <Image
      source={require('../../assets/images/icons/home-icon-3.png')}
      style={{ height: 32, width: 32 }}
    />
  </View>
);
const TotalUserIcon = (
  <Image
    source={require('../../assets/images/icons/dash-users.png')}
    style={{ height: 52, width: 52 }}
  />
);

const optionsIcon = (
  <Image
    source={require('../../assets/images/icons/options.png')}
    style={{ height: 16, width: 16 }}
  />
);

// =-=-=-=-=-=-=-=-=-=-=-=-=-=-

const viewMoreIcon = (
  <Image
    source={require('../../assets/images/icons/view-more.png')}
    style={{ height: 16, width: 16 }}
  />
);
const editIcon = (
  <Image
    source={require('../../assets/images/icons/edit.png')}
    style={{ height: 28, width: 28 }}
  />
);
const editWIcon = (
  <Image
    source={require('../../assets/images/icons/editW.png')}
    style={{ height: 24, width: 24 }}
  />
);
const delIcon = (
  <Image
    source={require('../../assets/images/icons/del.png')}
    style={{ height: 28, width: 28 }}
  />
);
const delWIcon = (
  <Image
    source={require('../../assets/images/icons/delW.png')}
    style={{ height: 24, width: 24 }}
  />
);
const arrowRightIcon = (
  <Image
    source={require('../../assets/images/icons/arrow-right.png')}
    style={{ height: 16, width: 16 }}
  />
);
const arrowLeftIcon = (
  <Image
    source={require('../../assets/images/icons/arrow-left.png')}
    style={{ height: 20, width: 20 }}
  />
);
const calendarIcon = (
  <Image
    source={require('../../assets/images/icons/calendar.png')}
    style={{ height: 22, width: 22 }}
  />
);
const usersIcon = (
  <Image
    source={require('../../assets/images/icons/users.png')}
    style={{ height: 25, width: 25 }}
  />
);
const addIcon = (
  <Image
    source={require('../../assets/images/icons/add.png')}
    style={{ height: 22, width: 22 }}
  />
);
const backIcon = (
  <Image
    source={require('../../assets/images/icons/back.png')}
    style={{ height: 22, width: 22 }}
  />
);
const cancelWIcon = (
  <Image
    source={require('../../assets/images/icons/cancel-w.png')}
    style={{ height: 20, width: 20 }}
  />
);
const cancelIcon = (
  <Image
    source={require('../../assets/images/icons/cancel.png')}
    style={{ height: 18, width: 18 }}
  />
);
const voucherIcon = (
  <Image
    source={require('../../assets/images/icons/voucher.png')}
    style={{ height: 18, width: 18 }}
  />
);
const voucherRupiahIcon = (
  <Image
    source={require('../../assets/images/icons/voucherRupiah.png')}
    style={{ height: 25, width: 25 }}
  />
);
const voucherPercentIcon = (
  <Image
    source={require('../../assets/images/icons/voucherPercent.png')}
    style={{ height: 25, width: 25 }}
  />
);
const targetIcon = (
  <Image
    source={require('../../assets/images/icons/award-target.png')}
    style={{ height: 25, width: 25 }}
  />
);
const payButtonIcon = (
  <Image
    source={require('../../assets/images/icons/pay-button.png')}
    style={{ height: 50, width: 50 }}
  />
);


export default class CustomIcon extends PureComponent {
  render() {
    const { 
      menu, 
      power, 
      add, 
      back, 
      voucher, 
      cancelW, 
      cancel, 
      notification, 
      broadcaster, 
      totalRedeem, 
      totalUser, 
      calendar, 
      activePromo, 
      options, 
      viewMore, 
      edit, 
      editW, 
      del, 
      delW, 
      arrowRight, 
      arrowLeft,
      voucherPercent,
      voucherRupiah,
      target,
      payButton,
      users
    } = this.props;

    if (menu) return menuIcon;
    if (notification) return notificationIcon;
    if (broadcaster) return BroadcasterIcon;
    if (totalRedeem) return TotalRedeemIcon;
    if (totalUser) return TotalUserIcon;
    if (activePromo) return ActivePromoIcon;
    if (options) return optionsIcon;
    if (viewMore) return viewMoreIcon;
    if (edit) return editIcon;
    if (editW) return editWIcon;
    if (del) return delIcon;
    if (delW) return delWIcon;
    if (arrowRight) return arrowRightIcon;
    if (arrowLeft) return arrowLeftIcon;
    if (calendar) return calendarIcon;
    if (add) return addIcon;
    if (back) return backIcon;
    if (cancelW) return cancelWIcon;
    if (cancel) return cancelIcon;
    if (voucher) return voucherIcon;
    if (power) return powerIcon;
    if (voucherPercent) return voucherPercentIcon;
    if (voucherRupiah) return voucherRupiahIcon;
    if (target) return targetIcon;
    if (payButton) return payButtonIcon;
    if (users) return usersIcon;

    return children || null;
  }
}


import React, { PureComponent } from 'react';
import { View, Image } from 'react-native';

const menuIcon = (
  <View style={{padding: 15, marginLeft: -10, backgroundColor: 'rgba(0,0,0,0)'}}>
    <Image
      source={require('../../assets/images/icons/menu.png')}
      style={{ height: 14, width: 18 }}
    />
  </View>
);

const notificationIcon = (
  <Image
    source={require('../../assets/images/icons/notifications.png')}
    style={{ height: 22, width: 21 }}
  />
);
const powerIcon = (
  <View style={{padding: 15, marginRight: -10}}>
    <Image
      source={require('../../assets/images/icons/power.png')}
      style={{ height: 22, width: 21 }}
    />
  </View>  
);

const ActivePromoIcon = (
  <Image
    source={require('../../assets/images/icons/dash-percent.png')}
    style={{ height: 52, width: 52 }}
  />
);
const BroadcasterIcon = (
  <Image
    source={require('../../assets/images/icons/dash-megaphone.png')}
    style={{ height: 52, width: 52 }}
  />
);
const TotalRedeemIcon = (
  <Image
    source={require('../../assets/images/icons/dash-reload.png')}
    style={{ height: 52, width: 52 }}
  />
);
const TotalUserIcon = (
  <Image
    source={require('../../assets/images/icons/dash-users.png')}
    style={{ height: 52, width: 52 }}
  />
);

const optionsIcon = (
  <Image
    source={require('../../assets/images/icons/options.png')}
    style={{ height: 16, width: 16 }}
  />
);

// =-=-=-=-=-=-=-=-=-=-=-=-=-=-

const viewMoreIcon = (
  <Image
    source={require('../../assets/images/icons/view-more.png')}
    style={{ height: 16, width: 16 }}
  />
);
const editIcon = (
  <Image
    source={require('../../assets/images/icons/edit.png')}
    style={{ height: 28, width: 28 }}
  />
);
const delIcon = (
  <Image
    source={require('../../assets/images/icons/del.png')}
    style={{ height: 28, width: 28 }}
  />
);
const delWIcon = (
  <Image
    source={require('../../assets/images/icons/delW.png')}
    style={{ height: 22, width: 22 }}
  />
);
const arrowRightIcon = (
  <Image
    source={require('../../assets/images/icons/arrow-right.png')}
    style={{ height: 16, width: 16 }}
  />
);
const arrowLeftIcon = (
  <Image
    source={require('../../assets/images/icons/arrow-left.png')}
    style={{ height: 20, width: 20 }}
  />
);
const calendarIcon = (
  <Image
    source={require('../../assets/images/icons/calendar.png')}
    style={{ height: 22, width: 22 }}
  />
);
const addIcon = (
  <Image
    source={require('../../assets/images/icons/add.png')}
    style={{ height: 22, width: 22 }}
  />
);
const backIcon = (
  <Image
    source={require('../../assets/images/icons/back.png')}
    style={{ height: 22, width: 22 }}
  />
);
const cancelWIcon = (
  <Image
    source={require('../../assets/images/icons/cancel-w.png')}
    style={{ height: 20, width: 20 }}
  />
);
const cancelIcon = (
  <Image
    source={require('../../assets/images/icons/cancel.png')}
    style={{ height: 18, width: 18 }}
  />
);
const voucherIcon = (
  <Image
    source={require('../../assets/images/icons/voucher.png')}
    style={{ height: 18, width: 18 }}
  />
);
const voucherRupiahIcon = (
  <Image
    source={require('../../assets/images/icons/voucherRupiah.png')}
    style={{ height: 25, width: 25 }}
  />
);
const voucherPercentIcon = (
  <Image
    source={require('../../assets/images/icons/voucherPercent.png')}
    style={{ height: 25, width: 25 }}
  />
);


export default class Icon extends PureComponent {
  render() {
    const { 
      menu, 
      power, 
      add, 
      back, 
      voucher, 
      cancelW, 
      cancel, 
      notification, 
      broadcaster, 
      totalRedeem, 
      totalUser, 
      calendar, 
      activePromo, 
      options, 
      viewMore, 
      edit, 
      del, 
      delW, 
      arrowRight, 
      arrowLeft,
      voucherPercent,
      voucherRupiah
    } = this.props;

    if (menu) return menuIcon;
    if (notification) return notificationIcon;
    if (broadcaster) return BroadcasterIcon;
    if (totalRedeem) return TotalRedeemIcon;
    if (totalUser) return TotalUserIcon;
    if (activePromo) return ActivePromoIcon;
    if (options) return optionsIcon;
    if (viewMore) return viewMoreIcon;
    if (edit) return editIcon;
    if (del) return delIcon;
    if (delW) return delWIcon;
    if (arrowRight) return arrowRightIcon;
    if (arrowLeft) return arrowLeftIcon;
    if (calendar) return calendarIcon;
    if (add) return addIcon;
    if (back) return backIcon;
    if (cancelW) return cancelWIcon;
    if (cancel) return cancelIcon;
    if (voucher) return voucherIcon;
    if (power) return powerIcon;
    if (voucherPercent) return voucherPercentIcon;
    if (voucherRupiah) return voucherRupiahIcon;

    return children || null;
  }
}

import React, { Component } from 'react'
import { Image, StyleSheet, View, Dimensions, TouchableOpacity } from 'react-native'
import Card from './Card';
import Text from './Text';
import ImagePicker from 'react-native-image-picker';
import * as theme from '../../constants/theme';

const { width } = Dimensions.get("window");
// More info on all the options is below in the API Reference... just some common use cases shown here
const options = {
	title: 'Select Picture',
//   customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
	storageOptions: {
		skipBackup: true,
		path: 'images',
	},
};

/**
 * The first arg is the options object for customization (it can also be null or omitted for default options),
 * The second arg is the callback which sends object: response (more info in the API Reference)
 */

export default class IMGpicker extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			imgSource : null
		}
	}
	onPressHandle(){
		console.log("touched")
		ImagePicker.showImagePicker(options, (response) => {
			console.log('Response = ', response);

			if (response.didCancel) {
				console.log('User cancelled image picker');
			} else if (response.error) {
				console.log('ImagePicker Error: ', response.error);
			} else {
				const source = { uri: response.uri };
				// You can also display the image using data:
				// const source = { uri: 'data:image/jpeg;base64,' + response.data };
				this.props.onChangeImage(response)
				this.setState({
					imgSource: response,
				});
			}
		});
	}

	render(){
		const { label, ...props } = this.props;
		const inputStyles = [
			styles.imgContainer,
		];
		console.log(this.state.imgSource)
		return (
			<View>
				<View style={styles.labelContainer}>
					<Text style={styles.label}>
					{label}
					</Text>
				</View>
				<TouchableOpacity onPress={() => {this.onPressHandle()}}>
					<View style={styles.imgContainer}>
						{
							this.state.imgSource==null ?
								 <Image
									style={styles.imgCenter}
									source={require('../../assets/images/icons/camera.png')}
								/> : <Image
								style={styles.imgCenter}
								source={{uri:this.state.imgSource.uri}}
							/>
							
						}
						
					</View>
				</TouchableOpacity>
			</View>	
		)

	}
}

const styles = StyleSheet.create({
	imgContainer: {
		height: 340,
		backgroundColor: theme.colors.white,
		borderRadius: 16,
		backgroundColor: theme.colors.white,
		shadowColor: theme.colors.shadow,
		shadowOpacity: 0.15,
		shadowRadius: 8,
		shadowOffset: { width: 0, height: 8 },
		elevation: 2,
		marginBottom: 25,
		justifyContent: 'center',
		alignItems: 'center',
		overflow: 'hidden'
	},
	imgCenter: {
		minHeight: 340,
		minWidth: width - 40,
		alignSelf: 'center',
		borderWidth: 16,
		borderColor: theme.colors.white,
	},
	label: {
		color: theme.colors.gray3
	},
	  labelContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginBottom: 8,
		marginLeft: 15,
		
	}
});
import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native'

import * as theme from '../../constants/theme';

export default class Block extends Component {
  render() {
    const { flex, disable, row, center, middle, right, space, color, style, children, ...props } = this.props;
    const blockStyles = [
      styles.block,
      flex && { flex },
      flex === false && { flex: 0 },
      center && styles.center,
      middle && styles.middle,
      right && styles.right,
      disable && styles.disable,
      space && { justifyContent: `space-${space}` },
      row && styles.row,
      color && styles[color], // predefined styles colors for backgroundColor
      color && !styles[color] && { backgroundColor: color },
      style,
    ];

    return (
      <View style={blockStyles} {...props}>
        {children}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  block: {
    flex: 1,
  },
  row: {
    flexDirection: 'row'
  },
  center: {
    alignItems: 'center'
  },
  middle: {
    justifyContent: 'center'
  },
  right: {
    justifyContent: 'flex-end'
  },
  disable: {
    opacity: 0.3,
    color: '#aaa',
    backgroundColor: 'rgba(0,0,0,0.3)',
    borderRadius: 8
  },
  accent: { backgroundColor: theme.colors.accent },
  primary: { backgroundColor: theme.colors.primary },
});

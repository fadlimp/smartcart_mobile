import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Text, StyleSheet, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Modal from "react-native-modal";


class NetworkFail extends React.Component {

    static navigationOptions = {
        //To hide the ActionBar/NavigationBar
        header: null,
        tabBarVisible: false,
    };

	render() {
        
		return (
            <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={styles.linearGradient}>
                <Modal isVisible={true}>
                    <View style={{ backgroundColor: '#fff', height: 125, width: '100%', borderRadius: 16, paddingTop: 45, paddingHorizontal: 25, paddingBottom: 25}}>
                    <TouchableOpacity 
                        style={page.closeButton}
                        onPress={this.toggleModal}>
                        <Image
                            style={{width: 10, height: 10}}
                            source={require('../assets/icon-times-white.png')}
                        />
                    </TouchableOpacity>
                    <View style={page.rowButton}>
                        <TouchableOpacity style={page.ReceiptButton}>
                        <Text style={page.ReceiptButtonText}>Find Info</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[page.ReceiptButton, {backgroundColor: '#57789F'}]}>
                        <Text style={page.ReceiptButtonText}>Find Recipes</Text>
                        </TouchableOpacity>
                    </View>
                    </View>
                </Modal>
            </LinearGradient>
                
		)
	}
}


// Later on in your styles..
var styles = StyleSheet.create({
    linearGradient: {
      flex: 1,
      paddingLeft: 15,
      paddingRight: 15,
      borderRadius: 5
    },
    buttonText: {
      fontSize: 18,
      fontFamily: 'Gill Sans',
      textAlign: 'center',
      margin: 10,
      color: '#ffffff',
      backgroundColor: 'transparent',
    },
  });

const page = StyleSheet.create({

    rowButton: {
      width: '100%',
      alignItems: 'center',
      justifyContent: 'space-between',
      flexDirection: 'row',
    },
    ReceiptButton: {
      width: '47%',
      padding: 15,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#77599A',
      borderRadius: 8,
    },
    ReceiptButtonText: {
      fontSize: 17,
      fontFamily: 'CeraPro-Bold',
      color: '#ffffff',
    },
  
    top: {
      zIndex: 3,
    },
    closeButton: {
      height: 25,
      width: 25,
      backgroundColor: 'rgba(211,62,62,1)',
      borderRadius: 25,
      position: 'absolute',
      right: 7,
      top: 7,
      alignItems: 'center',
      justifyContent: 'center',
    },
  
    listTax: {
      marginTop: 5,
    },
    alignLeft: {
      textAlign: 'left',
      fontWeight: '700',
      fontSize: 18,
      color: '#2A3348',
    },
    alignRight: {
      textAlign: 'right',
      fontWeight: '700',
      fontSize: 18,
      color: '#2A3348',
    },
    total: {
      fontSize: 20,
      color: '#36A49F',
    },
  
    pageWrapper: {
      marginTop: -65,
      paddingTop: 0,
      position: 'relative',
      zIndex: 2,
      paddingBottom: 250,
      paddingHorizontal: 0,
      marginBottom: 50,
      height: '100%'
    },
    center: {
      paddingHorizontal: 30,
      marginHorizontal: 0,
    },
    header: {
      height: 75,
      alignItems: 'center',
      marginTop: 85,
      paddingBottom: 0,
    },
    headerTitle: {
      fontSize: 27,
      fontFamily: 'CeraPro-Bold',
      color: '#000',
    },
    headerDate: {
      fontSize: 15,
      fontFamily: 'CeraPro-Regular',
      color: '#000',
      letterSpacing: 1.2,
    },
    headerSubtitle: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    subtitle: {
      fontSize: 16,
      fontFamily: 'CeraPro-Regular',
      color: '#ffffff',
      marginLeft: 8,
      textShadowColor: 'rgba(0, 0, 0, 0.75)',
      textShadowOffset: {width: 0, height: 1},
      textShadowRadius: 1,
      letterSpacing: .8,
    },
    searchButtonWp: {
      justifyContent: 'flex-end',
    },
    searchButton: {
      flexDirection: 'row',
      height: 65,
      alignItems: 'center',
      backgroundColor: '#000',
      paddingHorizontal: 15,
      borderRadius: 8,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 4 },
      shadowOpacity: 0.3,
      shadowRadius: 4,
      elevation: 10,
    },
    text: {
      color: '#ffffff',
      fontSize: 16,
      marginLeft: 8,
    },
    // - - - -
   
  
  });
export default NetworkFail;
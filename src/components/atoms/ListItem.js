
import React, { Component } from 'react'
import { TouchableOpacity, StyleSheet, View, Dimensions } from 'react-native'
import DatePicker from 'react-native-datepicker'
import Text from './Text';
import * as theme from '../../constants/theme';
import { ThemeColors } from 'react-navigation';
 
const { width } = Dimensions.get("window");

export default class ListItem extends Component {
	constructor(props){
		super(props)
		this.state = {date:"2016-05-15"}
	}
 
	render(){

		const { style, label, full, active, ...props } = this.props;
		const inputStyles = [
			styles.container,
			active && styles.active,
			style,
		];

		return (
			<View>
				<View style={inputStyles} {...props}>
					<Text H3 style={styles.label}>
					{label}
					</Text>
				</View>
			</View>	
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingVertical: 16,
		paddingHorizontal: 24,
		backgroundColor: theme.colors.white,
		borderColor: 'rgba(0,0,0,0)',
		borderBottomColor: 'rgba(55,55,55,0.18)',
		color: theme.colors.black,
		borderWidth: 1,
		fontSize: theme.sizes.font,
		color: theme.colors.black
	},
	active: {
		backgroundColor: 'rgba(16, 169, 143, 0.32)',
		color: theme.colors.primary,
	}
});
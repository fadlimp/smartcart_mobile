import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, Image, StyleSheet, ImageBackground } from 'react-native';

const styles = StyleSheet.create({
  sliderBox: {
    paddingHorizontal: 0,
    marginHorizontal: 0,
    height: 210,
    position: 'relative',
    zIndex: 9,
    elevation: 5,
  },
  promoBoxNearby: {
    marginLeft: 35,
    position: 'relative',
    width: 160,
    height: 165,
    backgroundColor: 'rgba(0,0,0,0)',
  },
  promoBoxContent: {
    backgroundColor: 'rgba(255,255,255, .8)',
    borderRadius: 12,
    paddingHorizontal: 15,
    paddingTop: 55,
    paddingBottom: 15,
    position: 'relative',
    width: '100%',
    height: 130,
    marginTop: 35,
  },
  promoImage: {
    height: 70,
    width: 70,
    borderRadius: 6,
    position: 'absolute',
    top: -35,
    left: 15,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffffff',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 8 },
    shadowOpacity: 0.3,
    shadowRadius: 12,
    elevation: 12,
  },
  promoTitle: {
    fontSize: 17,
    color: '#2A3348',
    fontWeight: '700',
  },
  promoSubtitle: {
    fontSize: 10,
    color: '#36A49F',
    lineHeight: 12,
  },
  span: {
    position: 'absolute',
    right: 0,
    top: -15,
    paddingVertical: 3,
    paddingHorizontal: 8,
    backgroundColor: '#6E95E5',
    fontSize: 13,
    color: '#ffffff',
    fontWeight: '700',
    borderRadius: 25,
    borderBottomRightRadius: 4,
    zIndex: 9,
  },
  noDistance: {
    opacity: 0,
  },
  lastSlide: {
    marginRight: 23,
  },
});

const getStyles = ({
  lastSlide,
  noDistance,
}) => {
  const promoStyles = [styles.promoBoxNearby];
  const spanStyles = [styles.span];

  if (lastSlide) {
    promoStyles.push(styles.lastSlide);
  }

  if (noDistance) {
    spanStyles.push(styles.noDistance);
  }

  return { promoStyles, spanStyles }
}

class PromoBoxNearby extends React.Component {
  static propTypes = {
    lastSlide: PropTypes.bool,
    noDistance: PropTypes.bool,
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string.isRequired,
    distance: PropTypes.string.isRequired,
  };

  static defaultProps = {
    lastSlide: false,
    noDistance: false,
  };

	render() {
    const { title, subtitle, distance,image, lastSlide, noDistance } = this.props;
    const { promoStyles, spanStyles } = getStyles({ lastSlide, noDistance });
    if (image == 1) {
      var imagePath = require('../assets/brand-unilever.png')
    } else if(image == 2){
      var imagePath = require('../assets/png_logo.png')
    }else if(image == 3){
      var imagePath = require('../assets/indofood.jpg')
    }
    
		return (
			<View View style={promoStyles}>
        <View style={styles.promoBoxContent}>
          <Text style={spanStyles}>{distance}</Text>
          <View style={styles.promoImage}>
            <Image
              style={{ height: 65, width: 65 }}
              source={imagePath}
            />
          </View>
          <Text style={styles.promoTitle}>{title}</Text>
          <Text style={styles.promoSubtitle}>{subtitle}</Text>
        </View>
      </View>
		)
	}
}

export default PromoBoxNearby;


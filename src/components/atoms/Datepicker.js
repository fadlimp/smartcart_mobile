
import React, { Component } from 'react'
import { TouchableOpacity, StyleSheet, View, Dimensions } from 'react-native'
import DatePicker from 'react-native-datepicker'
import Text from './Text';
import * as theme from '../../constants/theme';
 
const { width } = Dimensions.get("window");

export default class Datepicker extends Component {
	constructor(props){
		super(props)
		this.state = {date:"2016-05-15"}
	}
 
	render(){

		const { label, full, ...props } = this.props;
		const inputStyles = [
			styles.DatePicker,
			full && styles.full,
		];

		return (
			<View>
				<View style={styles.labelContainer}>
					<Text style={styles.label}>
					{label}
					</Text>
				</View>
				<DatePicker
					style={inputStyles}
					date={this.state.date}
					mode="date"
					placeholder="select date"
					format="YYYY-MM-DD"
					minDate={this.state.date}
					// maxDate="2016-06-01"
					confirmBtnText="Confirm"
					cancelBtnText="Cancel"
					androidMode="spinner"
					customStyles={{
						dateIcon: {
							position: 'absolute',
							left: 0,
							top: -2,
							marginLeft: 0
						},
						dateInput: {
							marginLeft: 36,
							borderColor: 'transparent',
							marginTop: -10
						}
						// ... You can check the source to find the other keys.
					}}
					onDateChange={(date) => {this.setState({date: date})}}
				/>
			</View>	
		)
	}
}

const styles = StyleSheet.create({
	label: {
		color: theme.colors.gray3
	},
	  labelContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginBottom: 8,
		marginLeft: 15,
		
	},
	DatePicker: {
		flex: 1,
		width: width / 2 - 36,
		paddingVertical: 12,
		paddingHorizontal: 16,
		height: 55,
		borderRadius: 16,
		backgroundColor: theme.colors.white,
		shadowColor: theme.colors.shadow,
		shadowOpacity: 0.15,
		shadowRadius: 8,
		shadowOffset: { width: 0, height: 8 },
		elevation: 2,

		fontSize: theme.sizes.font,
		color: theme.colors.black
	},
	full: {
		width: width - 50,
	}
});
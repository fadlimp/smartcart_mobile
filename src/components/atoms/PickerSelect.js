
import React, { Component } from 'react'
import { Picker, StyleSheet, View, Dimensions } from 'react-native'
import Text from './Text';
import * as theme from '../../constants/theme';
 
const { width } = Dimensions.get("window");

export default class PickerSelect extends Component {
	
	render(){

		const { label, children, onValueChange, selectedValue, ...props } = this.props;
		const inputStyles = [
			styles.PickerWrapper,
		];

		return (
			<View>
				<View style={styles.labelContainer}>
					<Text style={styles.label}>
					{label}
					</Text>
       			</View>
				<View style={styles.pickerContainer}>
					<Picker style={inputStyles} selectedValue={selectedValue} onValueChange={onValueChange} >
						{children}
					</Picker>
				</View>	
			</View>	
		)
	}
}

const styles = StyleSheet.create({
	label: {
		color: theme.colors.gray3
	},
	  labelContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginBottom: 8,
		marginLeft: 15,
		
	},
	pickerContainer: {
		flex: 1,
		paddingVertical: 12,
		paddingHorizontal: 16,
		height: 45,
		borderRadius: 8,
		backgroundColor: theme.colors.white,
		shadowColor: theme.colors.shadow,
		shadowOpacity: 0.15,
		shadowRadius: 8,
		shadowOffset: { width: 0, height: 8 },
		elevation: 2,

		fontSize: theme.sizes.font,
		color: theme.colors.black
	},
	PickerWrapper: {
		marginTop: -15
	}
});
import React, { Component } from 'react'
import { StyleSheet, Dimensions, TouchableOpacity } from 'react-native'
import * as theme from '../../constants/theme';

const { width } = Dimensions.get('window');

export default class Button extends Component {
  render() {
    const { style, full, blue, small, danger, secondary, opacity, children, ...props } = this.props;
    const buttonStyles = [
      styles.button,
      small && styles.small,
      full && styles.full,
      blue && styles.blue,
      secondary && styles.secondary,
      danger && styles.danger,
      style,
    ];

    return (
      <TouchableOpacity
        style={buttonStyles}
        activeOpacity={opacity || 0.8}
        {...props}
      >
        {children}
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: theme.colors.primary,
    borderRadius: 16,
    height: 55,
    paddingVertical: 11,
    alignItems: 'center',
    justifyContent: 'center',
    maxWidth: 400,
    minWidth: 200,
    fontFamily: 'CeraPro-Bold',
  },
  full: {
    width: width - 40,
  },
  secondary: {
    backgroundColor: 'rgba(55,55,55,0.1)'
  },
  danger: {
    backgroundColor: theme.colors.red
  },
  blue: {
    backgroundColor: theme.colors.blue
  },
  small: {
    height: 45,
    borderRadius: 12
	}
});

import React, { Component } from 'react'
import { TouchableOpacity, StyleSheet } from 'react-native'

import Block from './Block';
import Text from './Text';
import CustomIcon from './CustomIcon';
import * as theme from '../../constants/theme';

export default class Card extends Component {
	static defaultProps = {
		shadow: true,
		border: true,
		title: null,
	}

	renderHeader = () => {
		const { title } = this.props;
		if (!title) return null;

		return (
			<Block row space="between" style={styles.header}>
				<Text caption>{title}</Text>
				<TouchableOpacity>
					{/* <Icon options /> */}
				</TouchableOpacity>
			</Block>
		)
	}

	render() {
		const { shadow, small, border, style, children, ...props } = this.props;
		const cardStyles = [
			styles.card,
			small && styles.small,
			shadow && styles.shadow,
			border && styles.border,
			style,
		];

		return (
			<Block style={cardStyles} {...props}>
				{this.renderHeader()}
				{children}
			</Block>
		)
	}
}

const styles = StyleSheet.create({
	card: {
		flex: 1,
		padding: 20,
		borderRadius: 16,
		backgroundColor: theme.colors.white,
	},
	header: {
		paddingBottom: 22,
		color: theme.colors.primary
	},
	border: {
		borderColor: theme.colors.card,
		borderWidth: 0,
	},
	shadow: {
		shadowColor: theme.colors.shadow,
		shadowOpacity: 0.15,
		shadowRadius: 8,
		shadowOffset: { width: 0, height: 8 },
		elevation: 2,
	},
	small: {
		borderColor: theme.colors.white,
		borderWidth: 5
	}
});

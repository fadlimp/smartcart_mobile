import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';

import History from "_screens/history/index";
import detailHistory from "_screens/history/detailHistory";

export default createStackNavigator(
	{
        History,
        detailHistory
	}
);
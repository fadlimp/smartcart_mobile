import React from "react";

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from "react-navigation-stack";
import Icon from 'react-native-vector-icons/Ionicons';

import HomeScreen from '_screens/dashboard/HomeScreen';
import DetailsScreen from '_screens/dashboard/DetailsScreen';
import ExploreScreen from '_screens/dashboard/ExploreScreen';

const HomeStack = createStackNavigator({
  Home: HomeScreen
});

HomeStack.navigationOptions = {
  tabBarLabel: "Home",
  tabBarIcon: ({ focused }) => (
    <Icon name="ios-home" color={color} size={26} />
  )
};

const LinksStack = createStackNavigator({
  Links: DetailsScreen
});

LinksStack.navigationOptions = {
  tabBarLabel: "Links",
  tabBarIcon: ({ focused }) => (
    <Icon name="ios-notifications" color={color} size={26} />
  )
};

const SettingsStack = createStackNavigator({
  Settings: ExploreScreen
});

SettingsStack.navigationOptions = {
  tabBarLabel: "Settings",
  tabBarIcon: ({ focused }) => (
    <Icon name="ios-notifications" color={color} size={26} />
  )
};

export default createBottomTabNavigator({
  HomeStack,
  LinksStack,
  SettingsStack
});
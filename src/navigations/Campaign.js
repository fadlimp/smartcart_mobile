import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';

import Campaign from "_screens/campaign/index";

export default createStackNavigator(
	{
		Campaign
	}
);
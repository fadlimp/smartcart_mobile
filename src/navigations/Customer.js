import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';

import Customer from "_screens/customer/index";
import insertCustomer from "_screens/customer/insertCustomer";

export default createStackNavigator(
  {
    Customer,
    insertCustomer,
  }
);
import React, { Component } from 'react';
import { View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

export default class CustomSidebarMenu extends Component {
	constructor() {
		super();
		//Setting up the Main Top Large Image of the Custom Sidebar
		this.profileImage =
			'http://dekodr.co.id/smartcart/asset/images/Logo-2.png';
		//Array of the sidebar navigation option with icon and screen to navigate
		//This screens can be any screen defined in Drawer Navigator in App.js
		//You can find the Icons from here https://material.io/tools/icons/
		this.items = [
			{
				navOptionThumb: 'home',
				navOptionName: 'Dashboard',
				screenToNavigate: 'Dashboard',
			},
		];
	}
	render() {
		return (
			<View style={styles.sideMenuContainer}>
				{/*Top Large Image */}
				<Image
					source={{ uri: this.profileImage }}
					style={styles.sideMenuProfileIcon}
				/>
				{/*Divider between Top Image and Sidebar Option*/}
				{/*Setting up Navigation Options from option array using loop*/}
				<View style={{ width: '100%', zIndex: 2 }}>
					{this.items.map((item, key) => (
						<TouchableOpacity onPress={() => {
							global.currentScreenIndex = key;
							this.props.navigation.navigate(item.screenToNavigate);
						}}>
							<View
								style={{
									flexDirection: 'row',
									alignItems: 'center',
									paddingTop: 15,
									paddingBottom: 15,
									marginRight: 20,
									marginLeft: 20,
									borderRadius: 10,
									backgroundColor: global.currentScreenIndex === key ? '#10A98F' : '#ffffff',
								}}
								key={key}>
								<View style={{ marginRight: 10, marginLeft: 20 }}>
									<Icon name={item.navOptionThumb} size={25}  style={{
										color: global.currentScreenIndex === key ? '#FFF' : 'black', }}
									/>
								</View>
								<Text
									style={{
										fontSize: 15,
										color: global.currentScreenIndex === key ? '#FFF' : 'black',
									}}
									>
									{item.navOptionName}
								</Text>
							</View>
						</TouchableOpacity>  
					))}
				</View>
				<View style={styles.accent2}>
					<Image
						source={require('../assets/images/Base/accent-bottom.png')}
						style={styles.accentImage}
					/>
				</View>	
			</View>
		);
	}
}
const styles = StyleSheet.create({
	sideMenuContainer: {
		width: '100%',
		height: '100%',
		backgroundColor: '#fff',
		alignItems: 'flex-start',
		paddingTop: 20,
		overflow: "hidden"
	},
	sideMenuProfileIcon: {
		width: 190,
		height: 45,
		marginTop: 30,
		marginLeft: 20,
		marginBottom: 50
	},
	accent2: {
		position: 'absolute',
		bottom: 0,
		left: 0,
		zIndex: 1
	},
	accentImage: {
		width: 240,
		height: 240,
	}
});
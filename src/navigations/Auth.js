import React from "react";
import { createStackNavigator } from "react-navigation-stack";

import Login from '_screens/login';

export default createStackNavigator(
  {
    Login
  },
  {
    defaultNavigationOptions: {
      headerShown: false
    }
  }
);

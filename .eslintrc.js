module.exports = {
  root: true,
  extends: '@react-native-community',
  plugins: ['import'],
  settings: {
    'import/resolver': {
      node: {
        paths: ['src'],
        alias: {
          // baseURL: 'http://35.197.143.136:8000/',
          // elasticURL: 'http://35.197.143.136:9200',
          _assets: './src/assets',
          _components: './src/components',
          _atoms: './src/components/atoms',
          _molecules: './src/components/molecules',
          _organisms: './src/components/organisms',
          _navigations: './src/navigations',
          _screens: './src/screens',
          _actions: './src/actions',
          _services: './src/services',
          _styles: './src/styles',
          _utils: './src/utils',
          _config: './src/config',
        },
      },
    },
  },
};